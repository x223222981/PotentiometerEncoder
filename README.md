# PotentiometerEncoder库

**PotentiometerEncoder** 是一个Arduino库，允许您轻松将任何360°电位器转换为编码器。该库包括启用卡尔曼滤波器以获取更平滑读数的选项。提供的示例演示了如何使用该库以及如何使用Ticker库进行定期更新。

## 安装

1. 下载 [SimpleKalmanFilter](https://github.com/denyssene/SimpleKalmanFilter) 库（版本0.1.0），并在Arduino IDE中安装它。
2. 下载 `PotentiometerEncoder` 库，并将其放置在Arduino库文件夹中。

## 使用方法

### 初始化

```c++
#include "PotentiometerEncoder.h"
#include <Ticker.h>

PotentiometerEncoder encoder(32);  // 将 '32' 替换为连接到电位器的模拟引脚
Ticker timer1;

void timer1_callback(void)
{
    encoder.update();
}

void setup()
{
    Serial.begin(115200);
    encoder.begin();
    timer1.attach_ms(50, timer1_callback);  // 每50毫秒更新一次编码器
}
```

### 主循环

```c++
void loop()
{
    delay(5);

    int currentPosition = encoder.getCurrentPosition();
    long accumulatedPosition = encoder.getAccumulatedPosition();

    static unsigned long lastPrintTime = 0;
    if (millis() - lastPrintTime > 100)
    {
        Serial.print("当前位置: ");
        Serial.print(currentPosition);
        Serial.print(" 累计位置: ");
        Serial.print(accumulatedPosition);
        Serial.print(" 圈数: ");
        Serial.print(encoder.getRevolutions());
        Serial.print(" 速度(r/min): ");
        Serial.println(encoder.getSpeed());

        lastPrintTime = millis();
    }
}
```

### 常用函数

- **PotentiometerEncoder(int pin, bool enableKalmanFilter = true):** 构造函数，用于初始化PotentiometerEncoder实例。
- **void begin(int maxRawValue = 255, unsigned char resolution = 8):** 使用可选的最大原始值和分辨率参数初始化电位器编码器。
- **void update():** 更新当前位置。在主循环中调用此函数。
- **int getCurrentPosition():** 获取电位器的当前位置。
- **long getAccumulatedPosition():** 获取累计位置。
- **void resetPosition(int32_t position):** 将累计位置重置为自定义值。
- **int getRevolutions():** 获取圈数。
- **float getSpeed():** 获取速度，单位为每分钟的旋转数（r/min）。适用于低转速。

## 概要

使用PotentiometerEncoder库，您可以轻松地将任何360°电位器转换为编码器，提供准确的位置、圈数和速度信息。该库包括一个可选的卡尔曼滤波器，以获得更平滑的读数。在您的项目中享受使用PotentiometerEncoder！
