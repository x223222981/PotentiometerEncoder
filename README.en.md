# PotentiometerEncoder Library

**PotentiometerEncoder** is an Arduino library that allows you to easily convert any 360° potentiometer into an encoder. The library includes the option to enable a Kalman filter for smoother readings. The example provided demonstrates how to use the library along with a Ticker library for periodic updates.

## Installation

1. Download the [SimpleKalmanFilter](https://github.com/denyssene/SimpleKalmanFilter) library (version 0.1.0) and install it in your Arduino IDE.
2. Download the `PotentiometerEncoder` library and place it in your Arduino libraries folder.

## Usage

### Initialization

```c++
#include "PotentiometerEncoder.h"
#include <Ticker.h>

PotentiometerEncoder encoder(32);  // Replace '32' with the analog pin connected to your potentiometer
Ticker timer1;

void timer1_callback(void)
{
    encoder.update();
}

void setup()
{
    Serial.begin(115200);
    encoder.begin();
    timer1.attach_ms(50, timer1_callback);  // Update the encoder every 50ms
}
```

### Main Loop

```c++
void loop()
{
    delay(5);

    int currentPosition = encoder.getCurrentPosition();
    long accumulatedPosition = encoder.getAccumulatedPosition();

    static unsigned long lastPrintTime = 0;
    if (millis() - lastPrintTime > 100)
    {
        Serial.print("Current Position: ");
        Serial.print(currentPosition);
        Serial.print(" Accumulated Position: ");
        Serial.print(accumulatedPosition);
        Serial.print(" Revolutions: ");
        Serial.print(encoder.getRevolutions());
        Serial.print(" Speed(r/min): ");
        Serial.println(encoder.getSpeed());

        lastPrintTime = millis();
    }
}
```

### PotentiometerEncoder API

- **PotentiometerEncoder(int pin, bool enableKalmanFilter = true):** Constructor to initialize the PotentiometerEncoder instance.
- **void begin(int maxRawValue = 255, unsigned char resolution = 8):** Initialize the potentiometer encoder with optional parameters for maximum raw value and resolution.
- **void update():** Update the current position. Call this in your loop.
- **int getCurrentPosition():** Get the current position of the potentiometer.
- **long getAccumulatedPosition():** Get the accumulated position.
- **void resetPosition(int32_t position):** Reset the accumulated position to a custom value.
- **int getRevolutions():** Get the number of revolutions.
- **float getSpeed():** Get the speed in rotations per minute (r/min). Suitable for low-speed applications.

## Summary

With the PotentiometerEncoder library, you can easily transform any 360° potentiometer into an encoder, providing accurate position, revolution count, and speed information. The library includes an optional Kalman filter for smoother readings. Enjoy using PotentiometerEncoder in your projects!
