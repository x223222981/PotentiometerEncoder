#include "PotentiometerEncoder.h"
// 引入定时器的库
#include <Ticker.h>

PotentiometerEncoder encoder(32);
Ticker timer1;

// 定时器中断函数，每隔x调用一次
void timer1_callback(void)
{
    encoder.update();
}

void setup()
{
    Serial.begin(115200);
    encoder.begin();
    // 避免过快的调用，每隔50ms调用一次
    timer1.attach_ms(50, timer1_callback);
}

void loop()
{
    delay(5);

    int currentPosition = encoder.getCurrentPosition();
    long accumulatedPosition = encoder.getAccumulatedPosition();

    // 每隔100ms打印一次
    static unsigned long lastPrintTime = 0;
    if (millis() - lastPrintTime > 100)
    {
        Serial.print("Current Position: ");
        Serial.print(currentPosition);
        Serial.print(" Accumulated Position: ");
        Serial.print(accumulatedPosition);
        Serial.print(" Revolutions: ");
        Serial.print(encoder.getRevolutions());
        Serial.print(" Speed(r/min): ");
        Serial.println(encoder.getSpeed());

        lastPrintTime = millis();
    }
}
