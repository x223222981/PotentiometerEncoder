#ifndef PotentiometerEncoder_h
#define PotentiometerEncoder_h

#include "Arduino.h"
#include <SimpleKalmanFilter.h>
// https://github.com/denyssene/SimpleKalmanFilter  version 0.1.0

class PotentiometerEncoder
{
public:
    // pin: 旋转编码器的引脚，需要支持模拟输入；
    // enableKalmanFilter: 是否启用卡尔曼滤波器
    PotentiometerEncoder(int pin, bool enableKalmanFilter = true);
    ~PotentiometerEncoder();
    // 用以初始化旋转电位器作为的编码器
    // maxRawValue: 旋转电位器的最大值，默认为255
    // resolution: 旋转电位器的分辨率，默认设置为8
    void begin(int maxRawValue = 255, unsigned char resolution = 8);
    // 更新当前的位置，需要在循环中调用
    void update();
    // 获取当前电位器的位置
    int getCurrentPosition();
    // 获取累计的位置
    long getAccumulatedPosition();
    // 自定义累计位置
    // position: 需要设置的的位置
    void resetPosition(int32_t position);
    // 获取旋转的圈数
    int getRevolutions();
    // 获取转速RPM(r/min)
    // 仅能用于低速状态下测速作为参考值
    float getSpeed();

private:
    int _pin;
    int _maxRawValue;
    int _lastValue;
    long _accumulatedPosition;
    bool _enableKalmanFilter;
    bool _reverse;
    SimpleKalmanFilter *kalmanFilter;
};

#endif
